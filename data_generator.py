import os
import skimage
import numpy as np
import tensorflow.keras as keras


class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'

    def __init__(self, list_IDs, labels, batch_size=32, dim=(32, 32, 32), n_channels=1,
                 n_classes=10, shuffle=True):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return 2
        # return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, y = self.__data_generation(list_IDs_temp)

        return (X, y)

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        # X : (n_samples, *dim, n_channels)
        'Generates data containing batch_size samples'
        # Initialization

        X = np.empty((self.batch_size, *self.dim,
                      self.n_channels), dtype=np.int16)
        #y = np.empty((self.batch_size,))
        y = np.empty((self.batch_size, *self.dim,
                      self.n_channels), dtype=np.int16)
        print(X.dtype, y.dtype)
        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
            X[i, ] = loadData('data/' + ID + ".png")
            # Store class

            y[i, ] = loadData('label/' + ID + ".png", isMask=True)
        # X = np.random.rand(self.batch_size, *self.dim, self.n_channels)
        # y = np.random.randint(10, size=self.batch_size)
        return X.astype(np.int16), y.astype(np.int16)


def loadData(image_path, target_size=(256, 256), isMask=False):
    img = skimage.io.imread(image_path, as_gray=True)
    if isMask:
        img = img / 255.
        img[img > 0.5] = 1
        img[img < 0.5] = 0
    img = skimage.transform.resize(img, target_size)
    img = np.reshape(img, img.shape+(1,))

    return img


def getPartition(trainNum=30, testNum=30):
    partition = {}
    list_IDs = []
    for i in range(256):
        list_IDs.append("train/%d" % (i % 30))

    partition["train"] = list_IDs

    list_IDs.clear()
    for i in range(256):
        list_IDs.append("test/%d" % (i % 30))
    partition["test"] = list_IDs
    return partition


def saveResult(save_path, npyfile):
    for i, item in enumerate(npyfile):
        img = item[:, :, 0]
        skimage.io.imsave(os.path.join(
            save_path, "%d_predict.png" % i*2), img[0])
        skimage.io.imsave(os.path.join(
            save_path, "%d_predict.png" % i*2+1), img[1])
