import os
import tensorflow as tf
import tensorflow.keras as keras
import numpy as np

from model import *
from data_generator import *

import skimage.io as io
import skimage.transform as trans

params = {'dim': (256, 256),
          'batch_size': 8,
          'n_classes': 2,
          'n_channels': 1,
          'shuffle': True}

partition = getPartition()
training_generator = DataGenerator(partition["train"], None, **params)
test_generator = DataGenerator(partition["test"], None, **params)


model = unet()

# model_checkpoint = keras.callbacks.ModelCheckpoint(
#     'unet_membrane.hdf5', monitor='loss', verbose=1, save_best_only=True)

model.fit_generator(training_generator, steps_per_epoch=2, epochs=2)


results = model.predict_generator(test_generator, 15, verbose=1)

saveResult("data/test", results)
